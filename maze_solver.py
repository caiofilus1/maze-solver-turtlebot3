#!/usr/bin/env python
import math
import time
import threading
import rclpy
from rclpy.node import Node
from geometry_msgs.msg import Twist, Point
from sensor_msgs.msg import LaserScan
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan


PROXIMITY_THRESHOLD = 0.25
PROXIMITY_WALL_THRESHOLD = PROXIMITY_THRESHOLD - 0.02

pub_ = None
regions_ = {
    'right': 0,
    'fright': 0,
    'front': 0,
    'fleft': 0,
    'left': 0,
}
state_ = 0
state_dict_ = {
    0: 'find the wall',
    1: 'turn left',
    2: 'follow the wall',
}


def clbk_laser(msg):
    print('print')
    global regions_
    regions_ = {
        'right':  min(min(msg.ranges[270:305]), 10),
        'fright': min(min(msg.ranges[306:341]), 10),
        'front':  min(min(msg.ranges[342: 359] + msg.ranges[0: 18]), 10),
        'fleft':  min(min(msg.ranges[19:54]), 10),
        'left':   min(min(msg.ranges[53:90]), 10),
    }

    take_action()


def take_action():
    global regions_
    regions = regions_
    msg = Twist()
    linear_x = 0
    angular_z = 0

    state_description = ''

    d = 0.3
    print(regions)
    if regions['front'] > d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'case 1 - nothing'
        change_state(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] > d:
        state_description = 'case 2 - front'
        change_state(1)
    elif regions['front'] > d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'case 3 - fright'
        change_state(2)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'case 4 - fleft'
        change_state(0)
    elif regions['front'] < d and regions['fleft'] > d and regions['fright'] < d:
        state_description = 'case 5 - front and fright'
        change_state(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] > d:
        state_description = 'case 6 - front and fleft'
        change_state(1)
    elif regions['front'] < d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'case 7 - front and fleft and fright'
        change_state(1)
    elif regions['front'] > d and regions['fleft'] < d and regions['fright'] < d:
        state_description = 'case 8 - fleft and fright'
        change_state(0)
    else:
        state_description = 'unknown case'
        print(regions)
    print(state_description)


def find_wall():
    msg = Twist()
    msg.linear.x = 0.15
    msg.angular.z = -1.
    return msg


def turn_left():
    msg = Twist()
    msg.angular.z = 1.5
    return msg


def follow_the_wall():
    global regions_

    msg = Twist()
    msg.linear.x = 0.15
    msg.angular.z = 0.1
    return msg


def change_state(state):
    global state_, state_dict_
    if state is not state_:
        print('Wall follower - [%s] - %s' % (state, state_dict_[state]))
        state_ = state


class LaserSub(Node):
    def __init__(self, callback):
        # Cria subscriber para scan
        super().__init__('lasersub')
        self.subscription = self.create_subscription(
            LaserScan,
            'scan',
            callback,
            rclpy.qos.qos_profile_sensor_data
        )
        self.msg = LaserScan()
        self.subscription

    def listener_callback(self, msg):
        self.msg = msg


def main():
    rclpy.init(args=None)
    #robo = mazeSolver()
    # robo.solveMaze()

    global pub_

    node = Node('reading_laser')

    pub_ = node.create_publisher(Twist, '/cmd_vel', 10)

    sub = LaserSub(clbk_laser)

    #thread = threading.Timer(0.2, lambda: rclpy.spin_once(sub))
    # thread.start()

    rate = node.create_rate(20, node.get_clock())
    prev_msg = None
    while True:
        rclpy.spin_once(sub)
        msg = Twist()
        if state_ == 0:
            msg = find_wall()
        elif state_ == 1:
            msg = turn_left()
        elif state_ == 2:
            msg = follow_the_wall()
            pass
        else:
            print('Unknown state!')
        if not prev_msg or msg.angular.z != prev_msg.angular.z or msg.linear.x != prev_msg.linear.x:
            # print(msg)
            pub_.publish(msg)
        prev_msg = msg
        # rate.sleep()


if __name__ == '__main__':
    main()
