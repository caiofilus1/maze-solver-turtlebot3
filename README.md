# Maze-Solver-Turtlebot3


## Getting started

Start gazebo
```
ros2 launch turtlebot3_gazebo turtlebot3_world.launch.py
```
start robot program
```
colcon build --packages-select maze
ros2 run maze main
```
